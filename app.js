require("dotenv").config();
const { MongoClient } = require("mongodb");
const express = require("express");
const bp = require("body-parser");
const app = express();
app.use(bp.urlencoded({ extended: true }));

const url = process.env.MONGODB_URL;
const client = new MongoClient(url);

const dbName = process.env.DB_NAME;

app.get("/mongodb", function (req, res) {
  console.log("posted");
  const os = req.query.os;
  const manufacturer = req.query.brand;

  main(os, manufacturer)
    .then(console.log)
    .catch(console.error)
    .finally(() => client.close());
});

async function main(os, manufacturer) {
  // Use connect method to connect to the server
  await client.connect();
  console.log("Connected successfully to MongoDB server");
  const db = client.db(dbName);
  const collection = db.collection(process.env.COLLECTION_NAME);
  console.log(os, manufacturer);
  const result = await collection.aggregate([
    { $match: { manufacturer: manufacturer, os_type: os } },
    {
      $group: {
        _id: "$country",
        count: { $sum: 1 },
      },
    },
  ]);

  await result.forEach(console.dir);

  return "done.";
}

app.listen(process.env.PORT_NUMBER, function () {
  console.log("server started running on " + process.env.PORT_NUMBER);
});
