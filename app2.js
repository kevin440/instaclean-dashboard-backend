require("dotenv").config();
const { MongoClient } = require("mongodb");
const express = require("express");
const bp = require("body-parser");
const app = express();
app.use(bp.urlencoded({ extended: true }));

const url = process.env.MONGODB_URL;
const client = new MongoClient(url);

const dbName = process.env.DB_NAME;

app.get("/mongodb", function (req, res) {
  console.log("posted");
  const os = req.query.os;
  const start = req.query.start;
  const end = req.query.end;

  main(start, end, os)
    .then(console.log)
    .catch(console.error)
    .finally(() => client.close());
});

async function main(start, end, os) {
  s = new Date(start);
  e = new Date(end);
  // Use connect method to connect to the server
  await client.connect();
  console.log("Connected successfully to MongoDB server");
  const db = client.db(dbName);
  const collection = db.collection(process.env.COLLECTION_NAME);
  console.log(os, s, e);
  const result = await collection.aggregate([
    { $match: { os_type: os, $and: [{ created_at: { $gte: s } }, { created_at: { $lt: e } }] } },
    {
      $group: {
        _id: "$kochavaAttributionData.country",
        count: { $sum: 1 },
      },
    },
    { $sort: { count: -1 } },
  ]);

  await result.forEach(console.dir);

  return "done.";
}

app.listen(process.env.PORT_NUMBER, function () {
  console.log("server started running on " + process.env.PORT_NUMBER);
});

// d = new Date("2021-01-01");
// console.log(d);
// await users.aggregate([
//   {
//     $match: {
//       $and: [{ _id: { $gte: ObjectId(startDateObjectId), $lte: ObjectId(endDateObjectId) } }, { password: { $exists: true } }],
//     },
//   },
//   { $lookup: { from: "deviceoinfos", localField: "_id", foreignField: "user_id", as: "deviceinfos" } },
//   { $group: { _id: "$deviceinfos.kochavaAttributionData.country", count: { $sum: 1 } } },
//   { $sort: { count: -1 } },
// ]);
